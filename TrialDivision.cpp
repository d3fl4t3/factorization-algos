#include "TrialDivision.h"
#include <cassert>

std::vector<BN> TrialDivision(BN n, BN b) {
	assert(!n.isEven());
	assert(!n.isPrime());
	assert(b > 1);
	std::vector<BN> result = {};
	for (BN q = n, dk = 2; dk <= b; ) {
		auto div_res = n.divide(dk);
		q = div_res.first;
		auto r = div_res.second;
		if (r == BN(0)) {
			result.push_back(dk);
			n = q;
			if (n == BN(1)) {
				return result;
			}
		}
		else {
			dk = dk.nextPrime();
		}
	}
	result.push_back(n);
	return result;
}