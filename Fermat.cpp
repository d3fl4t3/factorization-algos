#include "Fermat.h"
#include <cassert>

std::pair<BN, BN> Fermat(BN n) {
	assert(!n.isEven());
	assert(!n.isPrime());
	auto x = n.floorSqrt();
	if (x * x == n) {
		return std::make_pair(x, x);
	}
	auto end = (n + BN(9)) / 6;
	for (x = x + 1; x <= end; x = x + 1) {
		auto z = x * x - n;
		auto y = z.floorSqrt();
		if (y * y == z) {
			return std::make_pair(x + y, x - y);
		}
	}
	return std::make_pair(0, 0);
}