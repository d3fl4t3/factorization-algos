﻿#include <iostream>
#include <fstream>
#include <thread>
#include <functional>
#include <sstream>
#include "BN.h"
#include "PrintVector.h"
#include "TrialDivision.h"
#include "Olvey.h"
#include "Fermat.h"
#include "Pollard.h"

typedef std::pair<const char *, std::function<BN(BN)>> algo;

bool testAlgo(BN n1, BN n2, algo a, std::ofstream &logfile) {
    BN test_input = n1 * n2;
    BN test_output = a.second(test_input);
    bool is_ok = test_output == n1 || test_output == n2;
    std::stringstream msg;
    msg << (is_ok ? "OKAY" : "FAIL") << " | " << a.first << "(" << n1 << ", " << n2 << ") = " << test_output << std::endl;
    std::cout << msg.str();
    if (!is_ok) {
        logfile << msg.str();
        logfile.flush();
    }
    return is_ok;
}

void randTestAlgo(int iterations, int operandBitSize, algo a, std::ofstream &logfile, int &errors) {
    for (int i = 0; i < iterations; i++) {
        BN n1 = BN::randPrime(BN(3), BN(1) << operandBitSize);
        BN n2 = BN::randPrime(BN(3), BN(1) << operandBitSize);
        if (!testAlgo(n1, n2, a, logfile)) {
            errors++;
        }
    }
}

void randTestMultithread(int iterations, int threadNum, int operandBitSize, algo a, std::ofstream& logfile) {
    std::vector<std::thread> threads;
    std::vector<int> outputs;
    outputs.resize(threadNum);
    for (int i = 0; i < threadNum; i++) {
        threads.push_back(std::thread(randTestAlgo, iterations / threadNum, operandBitSize, a, std::ref(logfile), std::ref(outputs[i])));
    }
    for (auto& t : threads) {
        t.join();
    }
    int errors = 0;
    for (auto output : outputs) {
        errors += output;
    }
    std::cout << "Errors: " << errors << "/" << iterations << std::endl;
}

int main()
{
    algo trial_div = std::make_pair(
        "TrialDivision",
        [](BN n) -> BN { return TrialDivision(n, n.floorSqrt()).back(); }
    );
    auto olvey = std::make_pair(
        "Olvey",
        [](BN n) -> BN { return Olvey(n); }
    );
    auto fermat = std::make_pair(
        "Fermat",
        [](BN n) -> BN { return Fermat(n).first; }
    );
    auto pollard1 = std::make_pair(
        "Pollard1",
        [](BN n) -> BN { return Pollard1(n); }
    );
    auto pollard2 = std::make_pair(
        "Pollard2",
        [](BN n) -> BN { return Pollard2(n, n); }
    );
    std::ofstream logfile("log.txt", std::ios::app);

    // Аргументы randTestMultithread:
    // 1. кол-во итераций теста, 
    // 2. кол-во потоков, 
    // 3. половина кол-ва бит в числе, 
    // 4. алгоритм, 
    // 5. файл в который писать ошибки.

    //randTestMultithread(10, 4, 20, trial_div, logfile);
    //randTestMultithread(10, 8, 16, olvey, logfile);
    //randTestMultithread(10, 8, 16, fermat, logfile);
    randTestMultithread(10, 4, 34, pollard1, logfile);
    //randTestMultithread(100, 4, 8, pollard2, logfile);

    return 0;
}