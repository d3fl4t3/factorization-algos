#pragma once
#include "BN.h"

BN Pollard1(BN n);
BN Pollard2(BN n, BN b);