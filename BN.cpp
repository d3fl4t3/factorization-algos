#include "BN.h"
#include <iomanip>
#include <sstream>
#include <string>
#include <tuple>
#include <immintrin.h>
#include <cassert>
#include <random>

using namespace std;

BN::BN(BN_WORD val)
{
	if (val) {
		v = { val };
	}
	else {
		v = {};
	}
}

BN::~BN()
{
}

string BN::ToString()
{
	stringstream ss;
	string hexTable = "0123456789abcdef";
	for (size_t i = v.size() - 1; i != -1; i--) {
		string buf = "";
		for (int j = sizeof(BN_WORD) * 2 - 1; j >= 0; j--) {
			buf += hexTable[(v[i] >> j * 4) & 0xF];
		}
		ss << setfill('0') << setw(sizeof(BN_WORD) * 2) << buf;
	}
	if (v.size() == 0) {
		ss << "0";
	}
	return ss.str();
}

// *this - dividend
// ����, ��� 2, ������ 4.3.1 (� �����), �������� D
pair<BN, BN> BN::divide(BN div)
{
	BN quotient, remainder;

	BN dividend = *this;

	BN divisor = div;

	// special conditions
	if (divisor == BN(0)) {
		throw BigDivisionByZeroException();
	}
	if (divisor > *this) {
		return make_pair(BN(0), BN(*this));
	}
	if (divisor == *this) {
		return make_pair(BN(1), BN(0));
	}

	if (divisor.v.size() == 1) {
		auto result = divide(divisor.v[0]);
		return make_pair(result.first, BN(result.second));
	}

	BN base = BN(1) << BN::ws();

	// D1
	BN_WORD d;
	if (divisor.v.back() == (BN_WORD)-1) {
		// �������� ���������� ��� ��� ���������� �������
		// (BN_WORD)-1 + 1 == base
		d = 1;
	}
	else {
		d = (base / (divisor.v.back() + 1)).v[0];
	}

	auto n = divisor.v.size();
	auto m = dividend.v.size() - n;

	dividend = dividend * d;
	divisor = divisor * d;

	dividend.v.resize(m + n + 1);
	quotient.v.resize(m + 1);

	BN q_, r_;

	// D2 - D7
	for (size_t j = m; j != -1; j--) {
		// D3
		tie(q_, r_) = ((BN(dividend.v[j + n]) << BN::ws()) | dividend.v[j + n - 1]).divide(divisor.v[n - 1]);
		while (q_ == base || (q_ * divisor.v[n - 2] > ((r_ << BN::ws()) | v[j + n - 2]))) {
			q_ = q_ - 1;
			r_ = r_ + divisor.v[n - 1];
			if (r_.v.size() > 1) break;
		}

		// D4
		BN dividend_part;
		for (size_t i = j; i < j + n + 1; i++) {
			dividend_part.v.push_back(dividend.v[i]);
		}
		dividend_part.delete_zeroes();
		BN q_2 = q_ * divisor;
		if (q_2 > dividend_part) {
			// ��������� ��� �������������, �������� �������
			dividend_part.v.push_back(1);
			dividend_part = dividend_part - q_2;

			// D6
			q_ = q_ - 1;
			dividend_part = dividend_part + divisor;
		}
		else {
			dividend_part = dividend_part - q_2;
		}

		dividend_part.v.resize(n + 1);

		// ������� dividend_part ������� � dividend
		for (size_t i = j; i < j + n + 1; i++) {
			dividend.v[i] = dividend_part.v[i - j];
		}

		// ����������� q_
		quotient.v[j] = q_.v.size() ? q_.v[0] : 0;
	}

	dividend.delete_zeroes();
	quotient.delete_zeroes();

	// D8
	remainder = dividend / d;

	return make_pair(quotient, remainder);
}

// *this - dividend
pair<BN, BN_WORD> BN::divide(BN_WORD divisor)
{
	BN quotient;
	BN_WORD remainder = 0;

	// special conditions
	if (divisor == 0) {
		throw BigDivisionByZeroException();
	}

	if (divisor == 1) {
		return make_pair(BN(*this), 0);
	}

	for (size_t i = v.size() - 1; i != -1; i--) {
		// ����� ��� ������� (remainder, v[i]) �� divisor

		DOUBLE_BASE dividend = ((DOUBLE_BASE)remainder << BN::ws()) | v[i];
		BN_WORD result = dividend / divisor;
		remainder = dividend % divisor;

		/*
		BN_WORD result = v[i] << 1;
		BN_WORD carry = v[i] >> (BN::ws() - 1);
		BN_WORD temp_carry = 0;

		for (int j = 0; j < BN::ws(); j++)
		{
			temp_carry = remainder >> (BN::ws() - 1);
			remainder <<= 1;
			remainder |= carry;
			carry = temp_carry;

			if (carry == 0)
			{
				if (remainder >= divisor)
				{
					carry = 1;
				}
				else
				{
					temp_carry = result >> (BN::ws() - 1);
					result <<= 1;
					result |= carry;
					carry = temp_carry;
					continue;
				}
			}

			remainder -= divisor;
			remainder -= (1 - carry);
			carry = 1;
			temp_carry = result >> (BN::ws() - 1);
			result <<= 1;
			result |= carry;
			carry = temp_carry;
		}
		*/
		if (quotient.v.size() != 0 || result != 0) {
			quotient.v.insert(quotient.v.begin(), result);
		}
	}

	return make_pair(quotient, remainder);
}

BN BN::floorSqrt()
{
	BN x(*this);
	if (x == BN(0) || x == BN(1))
		return x;
	BN start = BN(1), end = x / BN(2), ans;
	while (start <= end)
	{
		auto mid = (start + end) / BN(2);
		if (mid * mid == x)
			return mid;
		if (mid * mid < x)
		{
			start = mid + BN(1);
			ans = mid;
		}
		else {
			end = mid - BN(1);
		}
	}
	return ans;
}

BN BN::floorCbrt()
{
	BN x(*this);
	if (x == BN(0) || x == BN(1))
		return x;
	BN start = BN(1), end = x / BN(2), ans;
	while (start <= end)
	{
		auto mid = (start + end) / BN(2);
		if (mid * mid * mid == x)
			return mid;
		if (mid * mid * mid < x)
		{
			start = mid + BN(1);
			ans = mid;
		}
		else {
			end = mid - BN(1);
		}
	}
	return ans;
}

BN BN::operator/(BN_WORD divisor) {
	return this->divide(divisor).first;
}

BN_WORD BN::operator%(BN_WORD divisor) {
	return this->divide(divisor).second;
}

BN BN::mul(BN_WORD a, BN_WORD b) {

	// (a1*x+a0)(b1*x+b0) == (a1*b1)*x^2 + (a0*b1+b0*a1)*x + (a0*b0)

	auto lft = [=](BN_WORD x) -> BN_WORD {
		return (x >> BN::ws() / 2);
	};
	auto rgt = [=](BN_WORD x) -> BN_WORD {
		return x & (((BN_WORD)1 << BN::ws() / 2) - 1);
	};

	return (BN(lft(a) * lft(b)) << BN::ws()) +
		(BN(lft(a) * rgt(b)) + BN(lft(b) * rgt(a)) << BN::ws() / 2) +
		BN(rgt(a) * rgt(b));
}

BN_WORD BN::mul(BN_WORD a, BN_WORD b, BN_WORD m) {
	if (a == 0) return 0;
	BN_WORD q = 0;
	if (a & 1) {
		return add(mul(a ^ 1, b, m), b, m);
	}
	else {
		return shl1(mul(a >> 1, b, m), m);
	}
}

BN_WORD BN::add(BN_WORD a, BN_WORD b, BN_WORD m)
{
	BN_WORD base_r = (BN_WORD)(-1) % m;
	if (base_r + 1 == m) { // ������������ ������� ��� ����������� ��������� ��� ���
		base_r = 0;
	}
	else {
		base_r++; // ������ ��������� ������� � �������
	}

	return (a + b < b) ? add((a + b) % m, base_r, m) : (a + b) % m;
}

BN_WORD BN::shl1(BN_WORD a, BN_WORD m)
{
	BN_WORD base_r = (BN_WORD)(-1) % m;
	if (base_r + 1 == m) { // ������������ ������� ��� ����������� ��������� ��� ���
		base_r = 0;
	}
	else {
		base_r++; // ������ ��������� ������� � �������
	}

	return (a >> (BN::ws() - 1)) ? add((a << 1) % m, base_r, m) : (a << 1);
}

BN BN::operator=(BN_WORD val) {
	if (val) {
		v = { val };
	}
	else {
		v = {};
	}
	return *this;
}

BN BN::operator+(BN bn)
{
	auto minNumber = (this->v.size() > bn.v.size()) ? bn : *this;
	auto maxNumber = (this->v.size() > bn.v.size()) ? *this : bn;
	BN result(maxNumber);
	bool carry = 0;
	size_t i = 0;
	for (i = 0; i < minNumber.v.size(); i++) {
		result.v[i] += minNumber.v[i] + carry;
		carry = (minNumber.v[i] > result.v[i] - carry);
	}
	for (; i < maxNumber.v.size() && carry; i++) {
		result.v[i] += 1;
		carry = (result.v[i] == 0);
	}
	if (carry) {
		result.v.push_back(1);
	}
	return result;
}

BN BN::operator-(BN bn)
{
	if (*this < bn) {
		throw exception("bad operands");
	}
	BN result(*this);
	bool borrow = 0;
	size_t i = 0;
	for (i = 0; i < bn.v.size(); i++) {
		result.v[i] -= bn.v[i] + borrow;
		borrow = (this->v[i] < bn.v[i] + borrow);
	}
	for (; i < this->v.size() && borrow; i++) {
		result.v[i]--;
		borrow = (result.v[i] == (BN_WORD)(-1));
	}
	result.delete_zeroes();
	return result;
}

BN BN::operator*(BN_WORD word)
{
	BN result;
	for (size_t i = 0; i < this->v.size(); i++) {
		result = result + (mul(this->v[i], word) << BN::ws() * i);
	}
	return result;
}

BN BN::operator*(BN bn)
{
	BN result;
	for (size_t i = 0; i < bn.v.size(); i++) {
		result = result + ((*this * bn.v[i]) << BN::ws() * i);
	}
	return result;
}

BN BN::operator/(BN bn)
{
	return divide(bn).first;
}

BN BN::operator%(BN bn)
{
	return divide(bn).second;
}

BN::BN(string hex)
{
	auto ws = sizeof(BN_WORD); // word size
	auto hws = ws * 2; // hex word size

	size_t fsd = hex.find_first_not_of("0", 0); // first significant digit offset

	auto processWord = [](string block, size_t start, size_t end) -> BN_WORD {
		string hexTable = "0123456789abcdef";
		BN_WORD tmp = 0;
		for (size_t i = start; i < end; i++) {
			tmp <<= 4;
			tmp |= hexTable.find(block[i], 0);
		}
		return tmp;
	};

	size_t i;
	for (i = hex.length() - hws; i > fsd; i -= hws) {
		v.push_back(processWord(hex, i, i + hws));
	}
	v.push_back(processWord(hex, fsd, i + hws));
}

bool BN::operator==(BN bn)
{
	return this->v == bn.v;
}

bool BN::operator!=(BN bn)
{
	return !(*this == bn);
}

bool BN::operator>=(BN bn)
{
	return *this > bn || *this == bn;
}

bool BN::operator>(BN bn)
{
	if (this->v.size() > bn.v.size()) {
		return true;
	}
	else if (this->v.size() < bn.v.size()) {
		return false;
	}
	else {
		for (size_t i = this->v.size(); i > 0; i--) {
			if (this->v[i - 1] > bn.v[i - 1]) return true;
			else if (this->v[i - 1] < bn.v[i - 1]) return false;
		}
		return false;
	}
}

bool BN::operator<=(BN bn)
{
	return !(*this > bn);
}

bool BN::operator<(BN bn)
{
	return *this <= bn && *this != bn;
}

ostream& operator<<(ostream& out, BN bn)
{
	return out << bn.ToString();
}

BN BN::operator&(BN bn)
{
	auto minNumber = (this->v.size() > bn.v.size()) ? bn : *this;
	auto maxNumber = (this->v.size() > bn.v.size()) ? *this : bn;
	BN result(minNumber);
	for (size_t i = 0; i < minNumber.v.size(); i++) {
		result.v[i] &= maxNumber.v[i];
	}
	result.delete_zeroes();
	return result;
}

BN BN::operator|(BN bn)
{
	auto minNumber = (this->v.size() > bn.v.size()) ? bn : *this;
	auto maxNumber = (this->v.size() > bn.v.size()) ? *this : bn;
	BN result(maxNumber);
	for (size_t i = 0; i < minNumber.v.size(); i++) {
		result.v[i] |= minNumber.v[i];
	}
	return result;
}

BN BN::operator^(BN bn)
{
	auto minNumber = (this->v.size() > bn.v.size()) ? bn : *this;
	auto maxNumber = (this->v.size() > bn.v.size()) ? *this : bn;
	BN result(maxNumber);
	for (size_t i = 0; i < minNumber.v.size(); i++) {
		result.v[i] ^= minNumber.v[i];
	}
	result.delete_zeroes();
	return result;
}

BN BN::operator>>(size_t shift)
{
	auto del_words_num = shift / (BN::ws());
	auto shift_num = shift % (BN::ws());
	BN result(*this);
	result.v.erase(result.v.begin(), result.v.begin() + del_words_num);
	if (!shift_num) return result;
	BN_WORD current_head = 0, next_head = 0;
	for (size_t i = result.v.size(); i > 0; i--) {
		current_head = next_head;
		next_head = result.v[i - 1] & (((BN_WORD)1 << shift_num) - 1);
		result.v[i - 1] = (current_head << (BN::ws() - shift_num)) | (result.v[i - 1] >> shift_num);
	}
	if (result.v.size() > 0 && !result.v.back()) {  // delete zeroes
		result.v.pop_back();
	}
	return result;
}

BN BN::operator<<(size_t shift)
{
	auto add_words_num = shift / (BN::ws());
	auto shift_num = shift % (BN::ws());
	BN result(*this);
	if (result.v.empty()) return result;  // don't shift zero
	result.v.insert(result.v.begin(), add_words_num, 0);
	if (!shift_num) return result;
	BN_WORD current_head = 0, next_head = 0;
	for (size_t i = add_words_num; i < result.v.size(); i++) {
		current_head = next_head;
		next_head = result.v[i] >> (BN::ws() - shift_num);
		result.v[i] = (result.v[i] << shift_num) | current_head;
	}
	if (next_head) {
		result.v.push_back(next_head);
	}
	return result;
}

// ����������� ��������
//BN BN::gcd(BN a, BN b) {
//	if (b == BN(0)) {
//		return a;
//	}
//	return gcd(b, a % b);
//}

// �������� �������� ������: https://www.geeksforgeeks.org/steins-algorithm-for-finding-gcd/
BN BN::gcd(BN a, BN b) {
	/* GCD(0, b) == b; GCD(a, 0) == a,
	   GCD(0, 0) == 0 */
	if (a == 0)
		return b;
	if (b == 0)
		return a;

	/*Finding K, where K is the
	  greatest power of 2
	  that divides both a and b. */
	int k;
	for (k = 0; ((a | b) & 1) == 0; ++k)
	{
		a = a >> 1;
		b = b >> 1;
	}

	/* Dividing a by 2 until a becomes odd */
	while ((a & 1) == 0)
		a = a >> 1;

	/* From here on, 'a' is always odd. */
	do
	{
		/* If b is even, remove all factor of 2 in b */
		while ((b & 1) == 0)
			b = b >> 1;

		/* Now a and b are both odd.
		   Swap if necessary so a <= b,
		   then set b = b - a (which is even).*/
		if (a > b)
			swap(a, b); // Swap u and v.

		b = (b - a);
	} while (b != 0);

	/* restore common factors of 2 */
	return a << k;
}

BN_WORD BN::floorLog(BN a, BN b) {
	BN_WORD result;
	BN d = b;
	for (result = 0; a > d; d = d * b, result++);
	return result;
}

BN BN::pow(BN x, BN y, BN mod)
{
	BN q(x), z(!y.isEven() ? (mod > 0 ? x % mod : x) : 1);
	y = y >> 1;
	while (y > 0) {
		bool y_bit = !y.isEven();
		y = y >> 1;
		q = mod > 0 ? q * q % mod : q * q;
		if (y_bit) {
			z = mod > 0 ? z * q % mod : z * q;
		}
	}
	return z;
}

BN BN::rand(BN from, BN to)
{
	assert(to > from);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<BN_WORD> distr;
	BN bound = to - from;
	BN max, unbound_rand;
	for (size_t i = 0; i < bound.v.size(); i++) {
		max.v.push_back(-1);
		unbound_rand.v.push_back(distr(gen));
	}
	return from + unbound_rand / (max / (bound + 1) + 1);
}

BN BN::randPrime(BN from, BN to)
{
	BN result;
	while (!result.isPrime()) {
		result = BN::rand(from, to);
	}
	return result;
}

BN BN::nextPrime()
{
	BN result = this->isEven() ? *this + BN(1) : *this + BN(2);
	while (!result.isPrime()) {
		result = result + BN(2);
	}
	return result;
}

bool BN::isEven()
{
	return *this == BN(0) || (this->v[0] & 1) == 0;
}

bool millerRabinInner(BN d, BN n)
{
	auto a = BN::rand(BN(2), n - BN(2));
	auto x = BN::pow(a, d, n);
	if (x == BN(1) || x == n - BN(1)) {
		return true;
	}
	while (d != n - BN(1))
	{
		x = (x * x) % n;
		d = d << 1;
		if (x == BN(1)) {
			return false;
		}
		if (x == n - BN(1)) {
			return true;
		}
	}
	return false;
}

bool millerRabinTest(BN n, char k) {
	auto d = n - BN(1);
	while (d.isEven()) {
		d = d >> 1;
	}
	for (char i = 0; i < k; i++) {
		if (!millerRabinInner(d, n)) {
			return false;
		}
	}
	return true;
}

bool BN::isPrime()
{
	if (*this == BN(0) || *this == BN(1)) return false;
	if (*this == BN(2) || *this == BN(3)) return true;
	if (this->isEven()) return false;
	return millerRabinTest(*this, 10);
}
