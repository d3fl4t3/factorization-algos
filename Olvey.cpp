#include "Olvey.h"
#include <cassert>

BN Olvey(BN n) {
	assert(!n.isEven());
	assert(!n.isPrime());
	BN d = (n.floorCbrt() << 1) + BN(1);
	auto div1 = n.divide(d), div2 = n.divide(d - BN(2));
	auto r1 = div1.second, r2 = div2.second;
	auto q = (div2.first - div1.first) * 4;
	auto s = n.floorSqrt();
	for (d = d + BN(2); d <= s; d = d + BN(2)) {
		auto r = (r1 << 1) + q;
		if (r2 >= r) {
			r = r + d;
			q = q + BN(4);
		}
		r = r - r2;
		while (r >= d) {
			r = r - d;
			q = q - BN(4);
		}
		if (r == BN(0)) {
			return d;
		}
		else {
			r2 = r1;
			r1 = r;
		}
	}
	return BN(0);
}