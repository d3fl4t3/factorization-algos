#include "Pollard.h"
#include <cassert>

BN Pollard1(BN n) {
	assert(!n.isEven());
	assert(!n.isPrime());
	BN a = 2, b = 2;
	auto f = [&n](BN x) { return (x * x + 1) % n; };
	BN d = 1;
	while (d == 1) {
		a = f(a), b = f(f(b));
		if (a == b) {
			return 0;
		}
		d = BN::gcd(a > b ? a - b : b - a, n);
	}
	return d;
}

BN Pollard2_inner(BN n, BN b) {
	auto a = BN::rand(2, n - 2);
	auto d = BN::gcd(a, n);
	if (d > 1) {
		return d;
	}
	BN q_i = 2;
	while (q_i <= b) {
		auto e_i = BN::floorLog(n, q_i);
		a = BN::pow(a, BN::pow(q_i, e_i, n), n);
		if (a == BN(1)) {
			return BN(0);
		}
		d = BN::gcd(a - 1, d);
		if (d > 1) {
			return d;
		}
		q_i = q_i.nextPrime();
	}
	return 1;
}

// 0 - �� ���������� ��������� �
// 1 - ������� b ������� ����
BN Pollard2(BN n, BN b) {
	assert(!n.isEven());
	assert(!n.isPrime());
	assert(b > 1);
	BN result;
	for (char i = 0; i < 100 && result == 0; i++) {
		result = Pollard2_inner(n, b);
	}
	return result;
}