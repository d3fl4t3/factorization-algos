#pragma once
#include <vector>
#include <string>

typedef uint32_t BN_WORD;
typedef uint64_t DOUBLE_BASE;

struct BigDivisionByZeroException : public std::exception {};

class BN
{
public:
	~BN();

	// assignment
	BN operator=(BN_WORD val);

	// arithmetic operations
	BN operator+(BN bn);
	BN operator-(BN bn);
	BN operator*(BN_WORD bn);
	BN operator*(BN bn);
	BN operator/(BN bn);
	BN operator/(BN_WORD bn);
	BN operator%(BN bn);
	BN_WORD operator%(BN_WORD bn);

	// conversion constructors
	BN(std::string hex);
	BN(BN_WORD val = 0);

	// comparing
	bool operator==(BN bn);
	bool operator!=(BN bn);
	bool operator>=(BN bn);
	bool operator>(BN bn);
	bool operator<=(BN bn);
	bool operator<(BN bn);

	// binary operations
	BN operator&(BN bn);
	BN operator|(BN bn);
	BN operator^(BN bn);
	BN operator>>(size_t shift);
	BN operator<<(size_t shift);

	// representation
	friend std::ostream & operator<<(std::ostream & out, BN bn);
	std::string ToString();

	// �������
	std::pair<BN, BN> divide(BN divisor);

	// ������� �� �����
	std::pair<BN, BN_WORD> divide(BN_WORD divisor);

	// ����� ����� ����������� �����
	BN floorSqrt();

	// ����� ����� ����������� �����
	BN floorCbrt();

	// ���
	static BN gcd(BN a, BN b);

	// floor(log(a, b))
	static BN_WORD floorLog(BN a, BN b);

	// ���������� � ������� �� ������
	static BN pow(BN base, BN exp, BN mod = 0);

	// ��������� ����� � ���������� [from, to]
	static BN rand(BN from, BN to);

	// ��������� ������� ����� � ���������� [from, to]
	static BN randPrime(BN from, BN to);

	// ��������� �� ������� ������� �����
	BN nextPrime();

	// �������� �� ��������
	bool isEven();

	// �������� �� �������� (10 �������� �������-������)
	bool isPrime();

	// ������ ����� � �����
	static inline BN_WORD ws() {
		return sizeof(BN_WORD) * 8;
	}

private:
	std::vector<BN_WORD> v;

	// ��������� ���� ����
	static BN mul(BN_WORD a, BN_WORD b);

	void delete_zeroes() {
		for (size_t i = v.size() - 1; i != -1 && v.back() == 0; i--) {
			v.pop_back();
		}
	}

public:
	// ��������� ���� ���� �� ������
	static BN_WORD mul(BN_WORD a, BN_WORD b, BN_WORD m);
	
	// �������� ���� ���� �� ������
	static BN_WORD add(BN_WORD a, BN_WORD b, BN_WORD m);

	// ����� ����� ����� �� ������
	static BN_WORD shl1(BN_WORD a, BN_WORD m);
};

